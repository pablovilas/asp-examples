require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do

  it 'responds with a 200 status' do
    get :index
    expect(response.status).to eq 200
    expect(response.content_type).to eq 'text/html'
  end

  it 'responds with a text/html content type' do
    get :index
    expect(response.content_type).to eq 'text/html'
  end

  it 'saves a new article' do
    post :create, params: { article: { title: 'Article X', body: 'Lorem ipsum'} }
    expect(response.status).to eq 200
    expect(response.content_type).to eq 'text/html'
  end

end