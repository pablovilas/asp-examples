FactoryBot.define do

  factory :article do
    title { 'Article X' }
    body { 'Lorem ipsum dolor sit amet consectetur adipiscing, elit sapien pulvinar sodales ridiculus lobortis sem, duis augue metus velit odio. Elementum hendrerit rutrum urna ridiculus ligula suspendisse, accumsan himenaeos commodo duis ullamcorper cras dapibus' }
  end

end