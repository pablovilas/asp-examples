require 'rails_helper'

RSpec.describe Article, type: :model do

  before(:all) do
    Article.destroy_all
    @article = create(:article)
  end

  it 'is valid with valid attributes' do
    expect(@article).to be_valid
  end

  it 'has a unique title' do
    article2 = build(:article)
    expect(article2).to_not be_valid
  end

end